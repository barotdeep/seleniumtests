package kirklands;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

/*
 * GRI @2014
 * Test Cases for Login
 * Pre-condition : Login page should be Open
 */
public class TestsForLogin {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private String validUsername = "x1@gmail.com";
  private String validPassword = "validpassword";
  private String invalidUsername = "invalidEmailAddress";
  private String invalidPassword = "inval";

  @Before
  public void setUp() throws Exception {
	  	driver = new FirefoxDriver();
	  	baseUrl = "http://krkocp.com:8080/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get(baseUrl + "/");
	    if(isElementPresent(By.id("EmailSignupContainer"))){
	    	driver.findElement(By.linkText("Close")).click();
	    }
	    clickSignInLink();
  }
  
  /*
   * TC_Log_01
   * To validate the "Login" functionality with valid Email Address and Password.
   * 1) Click on Sign In option in the header section. 2) Enter valid "Email Address". 3)Enter valid "Password". 4)Click on "Sign In" button.
   * User should be logged in to the "My Account" page.
   */
  @Test
  public void testTCLog01() throws Exception {
	    enterDataToSignIn(validUsername, validPassword);
	    clickSignInButton();
	    loggedIntoAccountSuccessfully();
  }
  
  /*
   * TC_Log_02
   * To validate the "Login" functionality for case sensitivity of "Password" field.
   * 1) Enter valid  "Email Address". 2) Enter "Password" with changed case. 3) Click on "Sign In" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCLog02() throws Exception {
	  	enterDataToSignIn(validUsername, "validPassword");
	    clickSignInButton();
	    validationForInvalidFields();
	    //gives system error
  }
  
  /*
   * TC_Log_03
   * To validate if "Password" field is displayed in encrypted (*) format.
   * Enter data on the "Password" field.
   * "Password" should be not displayed in plain text format.
   */
  @Test
  public void testTCLog03() throws Exception {
	  	isElementPresent(By.cssSelector("input[id='password'][type='password']"));
	  	enterDataToSignIn(validUsername, validPassword);
  }
  
  /*
   * TC_Log_04
   * To validate the "Login" functionality with invalid Email Address/Password.
   * 1) Enter invalid "Email Address". 2) Enter invalid "Password". 3) Click on "Sign In" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCLog04() throws Exception {
	  	enterDataToSignIn(invalidUsername, invalidPassword);
	    clickSignInButton();
	    validationForInvalidEmailAddress();
	    enterDataToSignIn(invalidUsername, validPassword);
	    clickSignInButton();
	    validationForInvalidEmailAddress();
	    enterDataToSignIn(validUsername, invalidPassword);
	    clickSignInButton();
	    validationForInvalidFields();
	    //gives system error
  }
  
  /*
   * TC_Log_05
   * To validate the "Login" functionality when Email Address and Password fields are kept empty.
   * 1) Do not enter any data in "Email Address" and/or "Password" fields. 2) Click on "Sign In" button
   * Validation message should be displayed.
   */
  @Test
  public void testTCLog05() throws Exception {
	  enterDataToSignIn("", "");
	    clickSignInButton();
	  	validationForEmptyFields();
  }
  
  /*
   * TC_Log_06
   * To validate the "Login" functionality when user logs in with the old Password after Password has been changed.
   * 1) Change the Password for the current user. 2) Enter the valid Email Address. 3) Enter the old password. 4) Click on "Sign In" button
   * Validation message should be displayed.
   */
  //@Test
  public void testTCLog06() throws Exception {
	  	// Not able to reset password
  }
  
  /*
   * TC_Log_07
   * To validate login functionality for already logged in account in another instance of the same browser on the same machine.
   * 1) Sign In to an account from one instance of the browser. 2) While signed in, start another instance of browser and try to login with valid Email Address and Password for the same account
   * Should allow to log in the user.
   */
  @Test
  public void testTCLog07() throws Exception {
	  testTCLog01();
	  setUp();
	  testTCLog01();
	  driver.close();
  }
  
  /*
   * TC_Log_08
   * To validate login functionality for already logged in account in a new tab of the same browser on the same machine.
   * 1) Sign In to an account from one instance of the browser. 2) While logged in, open a new tab on the same browser and enter the same URL.
   * Home page of already logged in user should be displayed.
   */
  @Test
  public void testTCLog08() throws Exception {
	  	testTCLog01();
	  	String currentTab = driver.getWindowHandle();
	  	String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	  	driver.findElement(By.linkText("My Account")).sendKeys(selectLinkOpeninNewTab);
	  	Thread.sleep(10);
	  	ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(newTab.get(0));
	  	//Problem while switching tabs (Changing focus)
	  	loggedIntoAccountSuccessfully();
	  	driver.close();
	  	driver.switchTo().window(currentTab);
	    Thread.sleep(10);
  }
  
  /*
   * TC_Log_09
   * To validate the "Password" text not copied.
   * 1) Type a valid Password and copy it. 2) Try to paste it in some text editor
   * Should not allow to copy the Password.
   */
  @Test
  public void testTCLog09() throws Exception {
	  	enterDataToSignIn(validUsername, validPassword);
	  	String selectAll = Keys.chord(Keys.CONTROL, "a");
	  	driver.findElement(By.id("password")).sendKeys(selectAll);
	  	String copy = Keys.chord(Keys.CONTROL,"c"); 
	  	driver.findElement(By.id("password")).sendKeys(copy);
	  	String paste = Keys.chord(Keys.CONTROL,"v"); 
	  	driver.findElement(By.id("userName")).clear();
		driver.findElement(By.id("userName")).sendKeys(paste);
		assertEquals("●●●●●●●●●●●●●", driver.findElement(By.id("userName")).getAttribute("value"));
  }
  
  /*
   * TC_Log_10
   * To validate the Email Address and Password do not get displayed on the URL.
   * 1) Log into the site with valid credentials. 2) Check the URL.
   * Email Address and Password should not be displayed on the URL.
   */
  @Test
  public void testTCLog10() throws Exception {
	  	enterDataToSignIn(validUsername, validPassword);
	  	String url = driver.getCurrentUrl();
	  	assertFalse(url.contains(validUsername));
	  	assertFalse(url.contains(validPassword));
  }
  
  /*
   * TC_Log_11
   * To validate the functionality of "Forgot Password" link.
   * 1) Click on the Forgot Password link.
   * "Forgot your Password" page should be displayed.
   */
  @Test
  public void testTCLog11() throws Exception {
	  	driver.findElement(By.linkText("Forgot your password?")).click();
	    Thread.sleep(10);
	    Set<String> windowId = driver.getWindowHandles();
	    Iterator<String> iterator = windowId.iterator();
	    
	    String mainWinId = iterator.next();
	    String newWinId = iterator.next();
	    
	    driver.switchTo().window(newWinId);
	    assertTrue(isElementPresent(By.className("forgotPassword")));
	    Thread.sleep(10);
	    driver.close();
	    
	    driver.switchTo().window(mainWinId);
	    assertTrue(isElementPresent(By.cssSelector("img[alt=\"Sign In\"]")));
	    Thread.sleep(10);
  }
  
  public void enterDataToSignIn(String username, String password){
	  driver.findElement(By.id("userName")).clear();
	  driver.findElement(By.id("userName")).sendKeys(username);
	  driver.findElement(By.id("password")).clear();
	  driver.findElement(By.id("password")).sendKeys(password);
}
  
  public void clickSignInButton(){
	  driver.findElement(By.id("use-log-sign-in-btn")).click();
	  try {
		Thread.sleep(100);
	} catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println("Error: After clicking Sign In button");
	}
  }
  
  public void validationForInvalidEmailAddress(){
	  assertEquals("Please enter a valid email address", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  public void validationForEmptyFields(){
	  assertEquals("Please enter your email address/password.", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  public void validationForInvalidFields(){
	  assertTrue(isElementPresent(By.cssSelector("div.errorText")));
  }
  
  public void loggedIntoAccountSuccessfully(){
	  assertFalse(isElementPresent(By.cssSelector("div.errorText")));
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Your Information\"]")));
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Preferences\"]")));	 
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Account Tools\"]")));
  }
  
  public void clickSignInLink(){
	  driver.findElement(By.xpath("//a[contains(text(),'Sign In')]")).click();
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Sign In\"]")));
  }
  

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
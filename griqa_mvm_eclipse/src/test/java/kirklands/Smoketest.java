package kirklands;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Smoketest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://stage2.kirklands.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void ExampleTest1() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.id("js-keyword")).clear();
    driver.findElement(By.id("js-keyword")).sendKeys("ball");
    driver.findElement(By.cssSelector("input.searchBtn")).click();
    driver.findElement(By.xpath("//div[@id='SearchMain']/div[2]/div/div/div[2]/a/span")).click();
    driver.findElement(By.name("quantity")).click();
    driver.findElement(By.name("quantity")).clear();
    driver.findElement(By.name("quantity")).sendKeys("2");
    driver.findElement(By.cssSelector("input.add-to-cart.fl")).click();
    assertEquals("2 Items in shopping cart", driver.findElement(By.cssSelector("div.head > span")).getText());
    driver.findElement(By.cssSelector("img[alt=\"Checkout\"]")).click();
    driver.findElement(By.name("submit")).click();
    driver.findElement(By.id("signIn")).click();
    assertEquals("We don't have the email address you entered in our records. Please try again.", driver.findElement(By.cssSelector("div.errorContainer")).getText());
  }
  

  @Test
  public void testExample2() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.xpath("//img[@alt='Collections & New Arrivals']")).click();
    driver.findElement(By.xpath("//img[@alt='Outdoor']")).click();
    driver.findElement(By.xpath("//img[@alt='Kids']")).click();
    driver.findElement(By.xpath("//img[@alt='Furniture']")).click();
    driver.findElement(By.xpath("//img[@alt='Mirrors & Wall Decor']")).click();
    assertEquals("Wall Décor - Wall Accents | Kirklands", driver.getTitle());
    driver.findElement(By.xpath("//img[@alt='Home Decor & Pillows']")).click();
    assertEquals("Shop Unique Home Decor & Home Furnishings | Kirklands", driver.getTitle());
    driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']")).click();
    assertEquals("Decorative Lamps - Unique Lamps | Kirklands", driver.getTitle());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

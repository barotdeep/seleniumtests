package kirklands;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

/*
 * GRI @2014
 * Test Cases for Logout
 * Pre-condition : 1) Registered User should be successfully logged in. 2) Your Account page should be open.
 */
public class TestsForLogout {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private String validUsername = "x1@gmail.com";
  private String validPassword = "validpassword";

  @Before
  public void setUp() throws Exception {
	  	driver = new FirefoxDriver();
	  	baseUrl = "http://krkocp.com:8080/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get(baseUrl + "/");
	    if(isElementPresent(By.id("EmailSignupContainer"))){
	    	driver.findElement(By.linkText("Close")).click();
	    }
	    clickSignInLink();
	    enterDataToSignIn(validUsername, validPassword);
	    clickSignInButton();
	    loggedIntoAccountSuccessfully();
  }
  
  /*
   * TC_Out_01
   * To validate the "Login" functionality with valid Email Address and Password.
   * 1) Click on Sign In option in the header section. 2) Enter valid "Email Address". 3)Enter valid "Password". 4)Click on "Sign In" button.
   * User should be logged in to the "My Account" page.
   */
//  @Test
  public void testTCOut01() throws Exception {
	  clickSignOutButton();
	  assertEquals("You have successfully signed out.", driver.findElement(By.cssSelector("div.instructions")).getText());
	  assertTrue(isElementPresent((By.xpath("//a[contains(text(),'Sign In')]"))));
  }
  
  
  /*
   * TC_Out_02
   * To validate logout functionality in same browser.
   * 1) Login to an account and open a new tab in same browser. 2) Logout from the newly opened tab. Perform operations in the first tab of same browser.
   * Should be allowed to perform the functionalities of guest only.
   */
//  @Test
  public void testTCOut02() throws Exception {
	  	String currentTab = driver.getWindowHandle();
	  	String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	  	driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']")).sendKeys(selectLinkOpeninNewTab);
	  	Thread.sleep(1000);
	  	ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(newTab.get(0));
	  	//Problem while switching tabs (Changing focus)
	  	testTCOut01();
	  	driver.close();
	  	driver.switchTo().window(currentTab);
	  	//What it mean by guest?
	    Thread.sleep(1000);
  }
  
  
  /*
   * TC_Out_03
   * To validate logout functionality in different browser.
   * 1) Login to an account and open a new browser window. 2) Logout from the newly opened window. Perform operations in the first browser window
   * Should be allowed to perform the functionalities of logged in user.
   */
  @Test
  public void testTCOut03() throws Exception {
	  	setUp();
	  	clickSignOutButton();
	    Set<String> windowId = driver.getWindowHandles();
	    Iterator<String> iterator = windowId.iterator();
	    
	    String mainWinId = iterator.next();
	    String newWinId = iterator.next();
	    
	    driver.switchTo().window(newWinId);
	    assertEquals("You have successfully signed out.", driver.findElement(By.cssSelector("div.instructions")).getText());
		assertTrue(isElementPresent((By.xpath("//a[contains(text(),'Sign In')]"))));
	    Thread.sleep(100);
	    driver.close();
	    
	    driver.switchTo().window(mainWinId);
	    assertTrue(isElementPresent(By.cssSelector("img[alt=\"Sign In\"]")));
	    Thread.sleep(100);
  }
  
  public void enterDataToSignIn(String username, String password){
	  driver.findElement(By.id("userName")).clear();
	  driver.findElement(By.id("userName")).sendKeys(username);
	  driver.findElement(By.id("password")).clear();
	  driver.findElement(By.id("password")).sendKeys(password);
  }
  
  public void clickSignOutButton(){
	  driver.findElement(By.xpath("//a[contains(text(),'(Sign Out)')]")).click();
  }
  
  public void clickSignInButton(){
	  driver.findElement(By.id("use-log-sign-in-btn")).click();
	  try {
		Thread.sleep(100);
	} catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println("Error: After clicking Sign In button");
	}
  }
  
  public void loggedIntoAccountSuccessfully(){
	  assertFalse(isElementPresent(By.cssSelector("div.errorText")));
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Your Information\"]")));
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Preferences\"]")));	 
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Account Tools\"]")));
  }
  
  public void clickSignInLink(){
	  driver.findElement(By.xpath("//a[contains(text(),'Sign In')]")).click();
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Sign In\"]")));
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
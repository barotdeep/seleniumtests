package kirklands;


import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

/*
 * GRI @2014
 * Test Cases for Create An Account
 * Pre-condition : Create an Account form should be Open
 */
public class TestsForCreateAccount {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private String validEmailAddress = "x7@gmail.com";
  private String validPassword = "validpassword";
  private String validZipCode = "48103";
  private String invalidEmailAddress = "invalidEmailAddress";
  private String invalidPassword = "inval";
  private String invalidZipCode = "480";

  @Before
  public void setUp() throws Exception {
	  driver = new FirefoxDriver();
	  baseUrl = "http://krkocp.com:8080/";
	    
	  // Base URL Form My Account Screen
	  // baseUrl = "http://krkocp:8080/user/login.jsp?dest=%2Fuser%2Fmain.jsp";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get(baseUrl + "/");
	    if(isElementPresent(By.id("EmailSignupContainer"))){
	    	driver.findElement(By.linkText("Close")).click();
	    }
	    clickMyAccountLink();
  }
  
  /*
   * TC_Reg_01
   * To validate the "Create An Account"  functionality with valid data.
   * 1) Enter valid data in all mandatory fields. 2) Click on "Create An Account" button.
   * System should display an information message for successful Account creation and user should receive a Confirmation e-mail.
   */
  @Test
  public void testTCReg01() throws Exception {
	  	enterDataToCreateAcount(validEmailAddress,validPassword,validZipCode);
	  	clickCreateAccountButton();
	  	createAccountSuccessfully();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_02
   * To validate the "Create An Account" functionality for invalid email address.
   * 1) Enter invalid data in "Email Address" field. 2) Enter valid data in other mandatory fields. 3) Click on "Create An Account" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCReg02() throws Exception {
	  	enterDataToCreateAcount(invalidEmailAddress,validPassword,validZipCode);
	  	clickCreateAccountButton();
	  	validationForInvalidEmailAddress();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_03
   * To validate the "Create An Account" functionality for case sensitivity of "Password" and "Confirm Password" fields.
   * 1) Enter data with the changed case in "Password ", "Confirm Password" fields. 2) Enter valid data in other mandatory fields. 3) Click on "Create An Account" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCReg03() throws Exception {
	  	enterDataToCreateAcount(validEmailAddress,validPassword,validZipCode);
	  	driver.findElement(By.id("verifyPassword")).clear();
		driver.findElement(By.id("verifyPassword")).sendKeys("ValidPassword");
	  	clickCreateAccountButton();
	  	validationForPasswordMismatch();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_04
   * To validate the "Create An Account" functionality for mismatch of  "Password" and "Re-Enter Password" fields.
   * 1) Enter mismatch data in "Password" and "Re-Enter Password" fields. 2) Enter valid data in other mandatory fields. 3) Click on "Create An Account" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCReg04() throws Exception {
	  	enterDataToCreateAcount(validEmailAddress,validPassword,validZipCode);
	  	driver.findElement(By.id("verifyPassword")).clear();
		driver.findElement(By.id("verifyPassword")).sendKeys("validpasswords");
	  	clickCreateAccountButton();
	  	validationForPasswordMismatch();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_05
   * To validate if "Password"/ "Re-Enter Password" is displayed in encrypted (*) form.
   * 1) Enter data in "Password"/ "Re-Enter Password" fields.
   * "Password"/ "Re-Enter Password" Should not be displayed as plain text.
   */
  @Test
  public void testTCReg05() throws Exception {
	  	isElementPresent(By.cssSelector("input[id='passwordNewMember'][type='password']"));
	  	isElementPresent(By.cssSelector("input[id='verifyPassword'][type='password']"));
	  	enterDataToCreateAcount(validEmailAddress,validPassword,validZipCode);
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_06
   * To validate the "Create An Account" functionality  when invalid data is entered in mandatory fields.
   * 1) Enter invalid data in mandatory fields.2) Click on "Create An Account" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCReg06() throws Exception {
	  	enterDataToCreateAcount(invalidEmailAddress,invalidPassword,invalidZipCode);
	  	clickCreateAccountButton();
	  	validationForInvalidEmailAddress();
	  	validationForInvalidPassword();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_07
   * To validate the "Create An Account" functionality when mandatory fields are kept empty.
   * 1) Do not enter any data in the mandatory fields (fields marked with *) 2) Click on "Create An Account" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCReg07() throws Exception {
	  	enterDataToCreateAcount("","","");
	  	clickCreateAccountButton();
	  	validationForInvalidEmailAddress();
	  	validationForEmptyPassword();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_08
   * To validate whether "Duplicate Registrations" are allowed or not.
   * 1) Enter valid data in mandatory fields. 2) Enter existing email address. 3) Click "Create An Account" button.
   * Validation message should be displayed.
   */
  @Test
  public void testTCReg08() throws Exception {
	  	enterDataToCreateAcount(validEmailAddress,validPassword,validZipCode);
	  	clickCreateAccountButton();
	  	validationForDuplicateEmailAddress();
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_11
   * To validate the functionality of "Remember Me" checkbox.
   * 1) Enter valid data in all mandatory fields. 2) Select the checkbox of "Remember Me". 3) Click on "Create An Account" button.
   * User would be signed in automatically, if the next time user visit the site.
   */
  @Test
  public void testTCReg11() throws Exception {
	  	enterDataToCreateAcount(validEmailAddress,validPassword,validZipCode);
	  	driver.findElement(By.xpath("(//label[@id='use-log-remember-me']/input)[2]")).click();
	  	clickCreateAccountButton();
	  	tearDown();
	  	setUp();
	  	assertEquals("Welcome, !", driver.findElement(By.cssSelector("li.first.welcome")).getText());
	    assertEquals("(Sign Out)", driver.findElement(By.linkText("(Sign Out)")).getText());
	    Thread.sleep(1000);
  }
  
  /*
   * TC_Reg_12
   * To validate the functionality of "What's this?" link.
   * 1) Click on  "What's this?" link.
   * Remember me info page should be displayed in popup.
   */
  @Test
  public void testTCReg12() throws Exception {
	  	driver.findElement(By.xpath("(//a[contains(@href, '/user/remember_me_info.jsp')])[2]")).click();
	  	assertEquals("Remember Me", driver.findElement(By.xpath("//h1")).getText());
	    assertEquals("Remember Me Check the \"Remember Me\" box to remain signed into Kirklands.com. The next time you visit the site, you will be signed in automatically.", driver.findElement(By.cssSelector("div.popupBody")).getText());
	    Thread.sleep(1000);
  }
  
  public void enterDataToCreateAcount(String emailAddress, String password, String zipCode){
	  	driver.findElement(By.id("emailAddress")).click();
	    driver.findElement(By.id("emailAddress")).clear();
	    driver.findElement(By.id("emailAddress")).sendKeys(emailAddress);
	    driver.findElement(By.id("reEnterEmailAddress")).clear();
	    driver.findElement(By.id("reEnterEmailAddress")).sendKeys(emailAddress);
	    driver.findElement(By.id("passwordNewMember")).clear();
	    driver.findElement(By.id("passwordNewMember")).sendKeys(password);
	    driver.findElement(By.id("verifyPassword")).clear();
	    driver.findElement(By.id("verifyPassword")).sendKeys(password);
	    driver.findElement(By.id("zipCode")).clear();
	    driver.findElement(By.id("zipCode")).sendKeys(zipCode);
	    driver.findElement(By.id("emailPreference")).click();
	    driver.findElement(By.id("emailPreference")).click();
  }
  
  public void clickCreateAccountButton(){
	  driver.findElement(By.id("use-log-cre-acc-btn")).click();
	  try {
		Thread.sleep(1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println("Error: After clicking create account button");
	}
  }
  
  public void validationForInvalidEmailAddress(){
	  assertEquals("Please enter a valid email address", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  public void validationForDuplicateEmailAddress(){
	  assertEquals("The email address you've entered is already associated with another account. Please sign in or enter a different email address. Please try again.", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  public void validationForPasswordMismatch(){
	  assertEquals("Password entries do not match. Please try again.", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  public void validationForInvalidPassword(){
	  assertEquals("Password is too short.", driver.findElement(By.cssSelector("li.passwordField > div.errorText")).getText());
  }
  
  public void validationForEmptyPassword(){
	  assertEquals("Please enter a password.", driver.findElement(By.cssSelector("li.passwordField > div.errorText")).getText());
  }
  
  public void createAccountSuccessfully(){
	  assertFalse(isElementPresent(By.cssSelector("div.errorText")));
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Your Information\"]")));
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Preferences\"]")));	 
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Account Tools\"]")));
  }
  
  public void clickMyAccountLink(){
	  driver.findElement(By.linkText("My Account")).click();
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Sign In\"]")));
  }
  

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}


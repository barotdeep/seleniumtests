package kirklands;


import java.awt.event.ActionEvent;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

/*
 * GRI @2014
 */
public class TestsForHomepage {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private String userEmail = "deepcabinwala@gmail.com";
  private String userPassword = "java2012";
  private String productName;
  private String itemNum;
  private double productPrice;
  private String secondProductName;
  private String secondItemNum;
  private double secondProductPrice;
  private int updatedProductQty = 3;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://krkocp:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.get(baseUrl + "/");
    if(isElementPresent(By.id("EmailSignupContainer"))){
    	driver.findElement(By.linkText("Close")).click();
    }
  }

/*
 * TC_Home_01
 * To verify whether the site logo is present on the Header.
 * Site logo should be present on the Header and displayed properly.  
 */
  @Test
  public void testTCHome01() throws Exception {
    assertTrue(isElementPresent(By.cssSelector("img.ie6png")));
  }
  
  
  /*
   * TC_Home_02
   * To validate the navigation of the site logo on the Header.
   * 1) It should refresh the page. 2) Again Homepage should open.
   */
  @Test
  public void testTCHome02() throws Exception {
	driver.findElement(By.cssSelector("img.ie6png")).click();
	//verify it is homepage or not
    assertTrue(isElementPresent(By.cssSelector("img.ie6png")));
  }
  
  /*
   * TC_Home_03
   * To verify whether the "My Account" link is present on the Header.
   * "My Account" link should be present on the Header.
   */
  @Test
  public void testTCHome03() throws Exception {
	  assertTrue(isElementPresent(By.linkText("My Account")));
  }
  
  /*
   * TC_Home_04
   * To validate the navigation of "My Account"" link when the user is logged in.
   * 1) Click on the "My Account" link at the Header section.
   * These sections should be displayed : Your Information, Account Tools, Preferences.
   */
  @Test
  public void testTCHome04() throws Exception {
	  //user login info is needed
	  driver.findElement(By.linkText("My Account")).click();
	  assertTrue(isElementPresent(By.linkText("My Account")));
  }
  
  /*
   * TC_Home_05
   * To validate the navigation of "My Account" link when the user is not logged in.
   * 1) Click on the "My Account" link at the Header section.
   * "Sign In or Create an Account" page should be displayed.
   */
  @Test
  public void testTCHome05() throws Exception {
	  driver.findElement(By.linkText("My Account")).click();
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Sign In\"]")));
  }
  
  /*
   * TC_Home_06
   * To validate the navigation of "View Cart" link when the user is not logged in.
   * Select "View Cart" option .
   * "Shopping Cart" page should be displayed.
   */
  @Test
  public void testTCHome06() throws Exception {
	  driver.findElement(By.cssSelector("#js-ucart > input[type=\"image\"]")).click();
	  assertTrue(isElementPresent(By.cssSelector("img[alt=\"Shopping Cart\"]")));
  }
  
  /*
   * TC_Home_07
   * To validate the functionality of "Sign In" button for blank data in mandatory fields when the user is not logged in.
   * "1) Click on the ""My Account"" link at the Header section. 2) Don't enter data in mandatory fields. 3) Select ""Sign In"" option ."
   * Validation message as "Please enter your email address/password" should be displayed.
   */
  @Test
  public void testTCHome07() throws Exception {
	  driver.findElement(By.linkText("My Account")).click();
	  driver.findElement(By.id("use-log-sign-in-btn")).click();
	  assertEquals("Please enter your email address/password.", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  
  /*
   * TC_Home_08
   * To validate the functionality of "Sign In" button for invalid data in mandatory fields when the user is not logged in.
   * "1) Click on the ""My Account"" link at the Header section. 2) Enter invalid data in mandatory fields. 3) Select ""Sign In"" option ."
   * Proper message should be displayed.
   */
  @Test
  public void testTCHome08() throws Exception {
	  driver.findElement(By.linkText("My Account")).click();
	  
	  // When proper email is not entered
	  driver.findElement(By.id("userName")).clear();
	  driver.findElement(By.id("userName")).sendKeys("invalidEmail");
	  driver.findElement(By.id("password")).clear();
	  driver.findElement(By.id("password")).sendKeys("invalidPw");
	  driver.findElement(By.id("use-log-sign-in-btn")).click();
	  assertEquals("Please enter a valid email address", driver.findElement(By.cssSelector("div.errorText")).getText());
	  
	  //Wrong email address entered
	  driver.findElement(By.id("userName")).clear();
	  driver.findElement(By.id("userName")).sendKeys("invalidEmail@gmail.com");
	  driver.findElement(By.id("use-log-sign-in-btn")).click();
	  assertEquals("We don't have the email address you entered in our records. Please try again.", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  /*
   * TC_Home_09
   * To validate the functionality of "Sign In" button for invalid data in mandatory fields when the user is not logged in.
   * "1) Click on the ""My Account"" link at the Header section. 2) Enter valid data in mandatory fields. 3) Select ""Sign In"" option ."
   * Proper message should be displayed.
   */
  @Test
  public void testTCHome09() throws Exception {
	  driver.findElement(By.linkText("My Account")).click();
	  
	  // Enter valid data and check valid text afterwards
	  driver.findElement(By.id("userName")).clear();
	  driver.findElement(By.id("userName")).sendKeys("invalidEmail");
	  driver.findElement(By.id("password")).clear();
	  driver.findElement(By.id("password")).sendKeys("invalidPw");
	  driver.findElement(By.id("use-log-sign-in-btn")).click();
	  assertEquals("Please enter a valid email address", driver.findElement(By.cssSelector("div.errorText")).getText());
  }
  
  /*
   * TC_Home_10
   * To verify whether the "View Cart" link is present on the Header.
   * 1) Verify the "View Cart" link at the Header section.
   * "View Cart" link should be present at the Header.
   */
  @Test
  public void testTCHome10() throws Exception {
	  assertTrue(isElementPresent(By.cssSelector("#js-ucart > input[type=\"image\"]")));
  }
  
  /*
   * TC_Home_11
   * To validate the navigation of "View Cart" link on the Header.
   * 1) Click on the "View Cart" link at the Header section.
   * Dropdown containing items in the Cart should be displayed. Note: If not items in Cart then "You have no items in your shopping cart." should get displayed.
   */
  @Test
  public void testTCHome11() throws Exception {
	  Actions action = new Actions(driver);
	  action.moveToElement(driver.findElement(By.xpath("(//input[@type='image'])[2]"))).perform();
	  assertTrue(isElementPresent(By.id("ucart")));
	  if(!(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")))){
		  assertEquals("You have no items in your Shopping Cart", driver.findElement(By.xpath("//div[@id='ucart']/div[2]/div/div[2]")).getText());
	  }
  }

  /*
   * TC_Home_12
   * To validate the functionality of link over name/Image of the product from Mini shopping cart.
   * "1) Add products in cart. 2) Click on Mini shopping cart. 3) Click on Name/Image link of the product."
   * Link should be redirected to product details page.
   */
  @Test
  public void testTCHome12() throws Exception {
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']"))).perform();
	  driver.findElement(By.xpath("//a[contains(text(),'Accent Lighting')]")).click();
	  driver.findElement(By.xpath("//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span")).click();
	  
	  String productName = driver.findElement(By.cssSelector("h1")).getText();
	  String itemNumber = driver.findElement(By.cssSelector("div.item-no")).getText();
	  
	  driver.findElement(By.xpath("(//input[@type='image'])[4]")).click();
	  
	  Actions actionCart = new Actions(driver);
	  actionCart.moveToElement(driver.findElement(By.xpath("(//input[@type='image'])[2]"))).perform();
	  assertTrue(isElementPresent(By.id("ucart")));
	  assertTrue(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")));
			  
	//assertEquals(itemNumber, driver.findElement(By.cssSelector("div.sku")).getText());
	//assertEquals(productName, driver.findElement(By.cssSelector("h2 > a")).getText());
	  driver.findElement(By.cssSelector("h2 > a")).click();
	  assertTrue(isElementPresent(By.className("details")));
	  assertEquals(productName, driver.findElement(By.cssSelector("h1")).getText());
  }
  
  /*
   * TC_Home_13
   * To validate the functionality of "Edit" link from Mini shopping cart.
   * 1) Add products in cart. 2) Mouse hover on Mini shopping cart at the Header section. 3)Click on Edit link.
   * Link should be redirected to product details page. (Product qty. Edit page)
   */
  @Test
  public void testTCHome13() throws Exception {
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']"))).perform();
	  driver.findElement(By.xpath("//a[contains(text(),'Accent Lighting')]")).click();
	  driver.findElement(By.xpath("//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span")).click();
	  
	  String productName = driver.findElement(By.cssSelector("h1")).getText();
	  String itemNumber = driver.findElement(By.cssSelector("div.item-no")).getText();
	  
	  driver.findElement(By.xpath("(//input[@type='image'])[4]")).click();
	  
	  Actions actionCart = new Actions(driver);
	  actionCart.moveToElement(driver.findElement(By.xpath("(//input[@type='image'])[2]"))).perform();
	  assertTrue(isElementPresent(By.id("ucart")));
	  assertTrue(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")));
			  
	//assertEquals(itemNumber, driver.findElement(By.cssSelector("div.sku")).getText());
	//assertEquals(productName, driver.findElement(By.cssSelector("//a[contains(text(),'"+productName+"')]")).getText());
	  driver.findElement(By.xpath("(//a[contains(text(),'edit')])[2]")).click();
	  assertTrue(isElementPresent(By.id("quickLookOverlay")));
//	  It opens product edit page, but giving error while verifying specific product
//	  assertEquals(productName, driver.findElement(By.xpath("//div[@id='quickLookOverlay']/div[2]/div/div[2]/h1/a")).getText());
//	  assertEquals(itemNumber, driver.findElement(By.xpath("//div[@id='quickLookOverlay']/div[2]/div/div[2]/div")).getText());
  }
  
  /*
   * TC_Home_14
   * To validate the functionality of "Remove" link from Mini shopping cart.
   * 1) Add products in cart. 2) Mouse hover on Mini shopping cart at the Header section. 3)Click on Remove link.
   * Product should be removed properly.
   */
  @Test
  public void testTCHome14() throws Exception {
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']"))).perform();
	  driver.findElement(By.xpath("//a[contains(text(),'Accent Lighting')]")).click();
	  driver.findElement(By.xpath("//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span")).click();
	  
	  String productName = driver.findElement(By.cssSelector("h1")).getText();
	  String itemNumber = driver.findElement(By.cssSelector("div.item-no")).getText();
	  
	  driver.findElement(By.xpath("(//input[@type='image'])[4]")).click();
	  
	  Actions actionCart = new Actions(driver);
	  actionCart.moveToElement(driver.findElement(By.xpath("(//input[@type='image'])[2]"))).perform();
	  assertTrue(isElementPresent(By.id("ucart")));
	  assertTrue(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")));
			  
	//assertEquals(itemNumber, driver.findElement(By.cssSelector("div.sku")).getText());
	//assertEquals(productName, driver.findElement(By.cssSelector("//a[contains(text(),'"+productName+"')]")).getText());
	  driver.findElement(By.xpath("//a[contains(text(),'remove')]")).click();
	  assertEquals("You successfully removed "+ productName +" .", driver.findElement(By.cssSelector("div.infoText")).getText());

  }
  
  
  /*
   * TC_Home_15
   * To validate the functionality for "Checkout" button from Mini shopping cart.
   * 1) Add products in cart. 2) Mouse hover on Mini shopping cart at the Header section. 3)Click on "Checkout" link.
   * Checkout page should be displayed.
   */
  @Test
  public void testTCHome15() throws Exception {
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']"))).perform();
	  driver.findElement(By.xpath("//a[contains(text(),'Accent Lighting')]")).click();
	  driver.findElement(By.xpath("//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span")).click();
	  
	  String productName = driver.findElement(By.cssSelector("h1")).getText();
	  String itemNumber = driver.findElement(By.cssSelector("div.item-no")).getText();
	  
	  driver.findElement(By.xpath("(//input[@type='image'])[4]")).click();
	  
	  Actions actionCart = new Actions(driver);
	  actionCart.moveToElement(driver.findElement(By.xpath("(//input[@type='image'])[2]"))).perform();
	  assertTrue(isElementPresent(By.id("ucart")));
	  assertTrue(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")));
			  
	//assertEquals(itemNumber, driver.findElement(By.cssSelector("div.sku")).getText());
	//assertEquals(productName, driver.findElement(By.cssSelector("//a[contains(text(),'"+productName+"')]")).getText());
	  driver.findElement(By.xpath("//img[@alt='Checkout']")).click();
	  assertTrue(isElementPresent(By.className("checkoutBasket")));
	  assertEquals(productName, driver.findElement(By.xpath("//table[@id='shopping-cart-items']/tbody/tr[2]/td[2]/h2/a")).getText());
  }
  
  /*
   * TC_Home_16
   * To validate the "View Shopping Cart" icon link under the "Cart" link at the Header section..
   * 1) Add products in cart. 2) Click on the "Cart" link at the Header section. 3) Click on the "View Shopping Cart" link.
   * User should be navigated to the "Shopping Cart" page.
   */
  @Test
  public void testTCHome16() throws Exception {
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']"))).perform();
	  driver.findElement(By.xpath("//a[contains(text(),'Accent Lighting')]")).click();
	  driver.findElement(By.xpath("//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span")).click();
	  
	  String productName = driver.findElement(By.cssSelector("h1")).getText();
	  String itemNumber = driver.findElement(By.cssSelector("div.item-no")).getText();
	  
	  driver.findElement(By.xpath("(//input[@type='image'])[4]")).click();
	  
	  //it pops up the window do not click directly
	  driver.findElement(By.xpath("//div[@id='js-ucart']/input")).click();
	  Thread.sleep(1000);
	  assertTrue(isElementPresent(By.className("checkoutBasket")));
	  assertEquals(productName, driver.findElement(By.xpath("//table[@id='shopping-cart-items']/tbody/tr[2]/td[2]/h2/a")).getText());
  }
  
  /*
   * TC_Home_17
   * To validate whether "Mini Shopping Cart" quantity and price is updating properly.
   * "1) Add multiple products to the cart. 2) Click on the ""View Cart"" link. 3) Update the quantity of the products.
	  4) Mouse hover on Mini shopping cart. 5) Observe the mini shopping cart quantity and price."
   * The cart Item quantity and price should be updated accordingly.
   */
  @Test
  public void testTCHome17() throws Exception {
	  addFirstProductToCart();
	  addSecondProductToCart();
	  clickViewCartButton();
	  updateFirstProductQty(updatedProductQty);
	  mouseHoverViewCartButton();
	  validateUpdatedProductQty();
	  Thread.sleep(1000);
  }
  
  public void addFirstProductToCart(){
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Lamps & Lighting']"))).perform();
	  driver.findElement(By.xpath("//a[contains(text(),'Accent Lighting')]")).click();
	  addProductToCart();
	  productName = driver.findElement(By.cssSelector("h1")).getText();
	  itemNum = driver.findElement(By.cssSelector("div.item-no")).getText();
	  String price = driver.findElement(By.className("priceDisplay")).getText();
	  productPrice = Double.parseDouble(price.substring(1));
  }
  
  public void addSecondProductToCart(){
	  Actions menuAction = new Actions(driver);
	  menuAction.moveToElement(driver.findElement(By.xpath("//img[@alt='Home Decor & Pillows']"))).perform();
	  driver.findElement(By.xpath("(//a[contains(text(),'Candle Holders')])[2]")).click();
	  addProductToCart();
	  secondProductName = driver.findElement(By.cssSelector("h1")).getText();
	  secondItemNum = driver.findElement(By.cssSelector("div.item-no")).getText();
	  String price = driver.findElement(By.className("priceDisplay")).getText();
	  secondProductPrice = Double.parseDouble(price.substring(1));
  }
  
  public void clickViewCartButton(){
	  driver.findElement(By.xpath("//div[@id='js-ucart']/input")).click();
	  if(isElementPresent(By.id("ucart"))){
		  driver.findElement(By.xpath("//a[contains(text(),'Close')]")).click();
		  clickViewCartButton();
	  }
	  assertTrue(isElementPresent(By.className("checkoutBasket")));
  }
  
  public void updateFirstProductQty(int updatedQty){
	  assertTrue(isElementPresent(By.className("checkoutBasket")));
	  assertEquals(productName, driver.findElement(By.xpath("//table[@id='shopping-cart-items']/tbody/tr[2]/td[2]/h2/a")).getText());
	  driver.findElement(By.xpath("//a[contains(text(),'Update')]")).click();
	  assertTrue(isElementPresent(By.id("quickLookOverlay")));
	  driver.findElement(By.id("quantity")).sendKeys(String.valueOf(updatedQty));
	  driver.findElement(By.cssSelector("input.update-to-cart")).click();
	  try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		e.printStackTrace();
		System.out.println("Error while updating first product qty");
	}
  }
  
  public void validateUpdatedProductQty(){
	  assertTrue(isElementPresent(By.id("ucart")));
	  assertTrue(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")));
	  
	  String updatedPrice = driver.findElement(By.xpath("//div[@id='ucart']/div[2]/div/div[3]/div/div/div[3]/div[2]/span[2]")).getText();
	  String updatedQty = driver.findElement(By.xpath("//div[@id='ucart']/div[2]/div/div[3]/div/div/div[3]/div[2]/span")).getText();
	  assertEquals(productName, driver.findElement(By.xpath("//a[contains(text(),'"+productName+"')]")));
	  assertEquals(productPrice*updatedProductQty, Double.parseDouble(updatedPrice.substring(1)),0.1);
	  assertEquals(updatedProductQty, Integer.parseInt(updatedQty.substring(11)));
  }
  
  public void mouseHoverViewCartButton(){
	  Actions action = new Actions(driver);
	  action.moveToElement(driver.findElement(By.xpath("(//input[@type='image'])[2]"))).perform();
	  assertTrue(isElementPresent(By.id("ucart")));
	  if(!(isElementPresent(By.xpath("//div[@id='ucart']/div[2]/div/div/span")))){
		  assertEquals("You have no items in your Shopping Cart", driver.findElement(By.xpath("//div[@id='ucart']/div[2]/div/div[2]")).getText());
	  }
  }
  
  
  public void addProductToCart(){
	  String availableItemPath = "//div[@id='SearchMain']/div[5]/div/div/div[3]/ul/li";
	  String productPath = "//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span";
	  String pricePath = "//div[@id='SearchMain']/div[5]/div/div/div[2]/a/span[2]";
	  int count = 2;
	  while(!isElementPresent(By.xpath("//div[@id='SearchMain']/div[5]/div/div/div[3]/ul/li"))){
		  availableItemPath = "//div[@id='SearchMain']/div[5]/div/div["+count+"]/div[3]/ul/li";
		  productPath = 	  "//div[@id='SearchMain']/div[5]/div/div["+count+"]/div[2]/a/span";
		  pricePath = 		  "//div[@id='SearchMain']/div[5]/div/div["+count+"]/div[2]/a/span[2]";
		  count++;
	  }
	  
	  if(isElementPresent(By.xpath(availableItemPath))){
		  driver.findElement(By.xpath(productPath)).click();
	  }
	  
	  driver.findElement(By.xpath("(//input[@type='image'])[4]")).click();
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}




import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class KirklandTotal {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://stage2.kirklands.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testKirklandTotal() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.cssSelector("li.catHover > a > img.ie6png")).click();
    driver.findElement(By.linkText("Kids Beds")).click();
    driver.findElement(By.cssSelector("span.result-title")).click();
    driver.findElement(By.cssSelector("input.add-to-cart.fl")).click();
    driver.findElement(By.linkText("Accent Chairs")).click();
    driver.findElement(By.cssSelector("span.result-title")).click();
    driver.findElement(By.cssSelector("div.buyControls")).click();
    driver.findElement(By.cssSelector("input.add-to-cart.fl")).click();
    driver.findElement(By.cssSelector("img[alt=\"Checkout\"]")).click();
    assertEquals("$149.99", driver.findElement(By.cssSelector("td.total")).getText());
    assertEquals("$199.99", driver.findElement(By.xpath("//table[@id='shopping-cart-items']/tbody/tr[3]/td[6]")).getText());
    assertEquals("$349.98", driver.findElement(By.id("estimated-grand-total")).getText());
    driver.findElement(By.cssSelector("#basket_body > div.submitContainer > input[name=\"submit\"]")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
